/**************************
  Carson Wallace
  wcwalla
  Lab 5
  Lab Section: 6
  Name of TA: Anurata Prabha Hridi
***************************/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

//Create a struct that contains the data for each card in the deck.
typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}

/*This function creates & initializes a deck of cards, creates a hand
hand from it, sorts the hand by suit and value, and then prints the hand.*/
int main(int argc, char const *argv[]) {
  
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/
   Card deck[52];
   int i = 0;
   int j = 0;

   //Initialize deck by organizing the cards with values 2-14 between 4 suits.
   for (i = 0; i < 4; i++) {
     for (j = 0; j < 13; j++) {

       //Define each suit per each increment of 13 cards
       switch (i) {
         case 0:
         deck[(13 * i) + j].suit = SPADES;
         break;
         case 1:
         deck[(13 * i) + j].suit = HEARTS;
         break;
         case 2:
         deck[(13 * i) + j].suit = DIAMONDS;
         break;
         case 3:
         deck[(13 * i) + j].suit = CLUBS;
         break;
       }

       //Define each card value between 2-14.
       deck[(13 * i) + j].value = j + 2;
     }
   }

  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
   random_shuffle(deck, (&deck[52]), myrandom);


   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/
  Card hand[5];

  //initialize each card in the hand with the top 5 cards of the shuffled deck.
  for (i = 0; i < 5; i++) {
    hand[i].suit = deck[i].suit;
    hand[i].value = deck[i].value;
  }

    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/

  Card temp;

  bool sorted = false;

  /*Iterate through each pair of cards in the deck and compare them.
  If the suit_order function returns false, switch the two cards. Do this until
  suit_order has iterated through the entire hand without returning false.*/
  while (sorted == false) {
    sorted = true;
    for (i = 0; i < 4; i++) {
      if (suit_order(hand[i], hand[i + 1]) == false) {
        sorted = false;
        temp = hand[i];
        hand[i] = hand[i + 1];
        hand[i + 1] = temp;
      }
    }
  }

  //For each card in the hand, print its suit and value.
  //If the value is 11 or above, print the name of the face card.
  for (i = 0; i < 5; i++) {
    if (hand[i].value > 10) {
      cout << right << setw(10) << get_card_name(hand[i])
      << " of " << get_suit_code(hand[i]) << endl;
    }
    else {
      cout << right << setw(10) << hand[i].value
      << " of " << get_suit_code(hand[i]) << endl;
    }
  }

  return 0;
}





/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/

/*This function compares two cards and determines which one has a higher value.
If the card on the left is larger than the one on the right, the function
returns false, indicating that they should swap positions.*/
bool suit_order(const Card& lhs, const Card& rhs) {

  /*The suit is checked first. The suits of the cards are compared per the
  enumeration defined above.*/
  if (lhs.suit > rhs.suit) {
    return false;
  }
  else if (lhs.suit < rhs.suit) {
    return true;
  }

  //If the cards are the same suit, compare thair value.
  else {
    if (lhs.value > rhs.value) {
      return false;
    }
    else {
      return true;
    }
  }
}

//This function returns the symbol to be printed for the respective suit.
string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

//This function returns the name to be printed for the respective face card.
string get_card_name(Card& c) {
 switch (c.value) {
   //Take in the card's value and print the respective string for it.
   case 11:    return "Jack";
   case 12:    return "Queen";
   case 13:    return "King";
   case 14:    return "Ace";
   default:    return "";
 }
}
